package com.example.kangaroo.greencars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PointActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;
    private ProgressDialog mLogProgress;
    private RadioButton radio_a,radio_b, radio_c,radio_d, radio_e,radio_f, radio_g,radio_h,radio_i,radio_j;
    private Button btnPoint;
    private Toolbar toolbar;
    private String temp_key;
    private RadioGroup rdoroo;
    private RadioButton rb;
    private String statusplayment, currentDate ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point);
        toolbar = findViewById(R.id.toolbar_feedback);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("            Greencar Stops");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        radio_a =findViewById(R.id.radio_a);
        radio_b =findViewById(R.id.radio_b);
        radio_c =findViewById(R.id.radio_c);
        radio_d =findViewById(R.id.radio_d);
        radio_e =findViewById(R.id.radio_e);
        radio_f =findViewById(R.id.radio_f);
        radio_g =findViewById(R.id.radio_g);
        radio_h =findViewById(R.id.radio_h);
        radio_i =findViewById(R.id.radio_i);
        radio_j =findViewById(R.id.radio_j);
        rdoroo = findViewById(R.id.rdoroo);
        btnPoint = findViewById(R.id.btnPoint);
        mAuth = FirebaseAuth.getInstance();
        mLogProgress = new ProgressDialog(this);
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat(" HH:mm:ss dd-MM-yyyy");
        String currentDate = formatter.format(calendar.getTime());


        rdoroo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                rb = rdoroo.findViewById(i);
                switch (i){

                    case R.id.radio_a:
                        statusplayment = rb.getText().toString();
                        break;

                    case R.id.radio_b:
                        statusplayment = rb.getText().toString();
                        break;
                    case R.id.radio_c:
                        statusplayment = rb.getText().toString();
                        break;

                    case R.id.radio_d:
                        statusplayment = rb.getText().toString();
                        break;
                    case R.id.radio_e:
                        statusplayment = rb.getText().toString();
                        break;

                    case R.id.radio_f:
                        statusplayment = rb.getText().toString();
                        break;
                    case R.id.radio_g:
                        statusplayment = rb.getText().toString();
                        break;

                    case R.id.radio_h:
                        statusplayment = rb.getText().toString();
                        break;

                    case R.id.radio_i:
                        statusplayment = rb.getText().toString();
                        break;

                    case R.id.radio_j:
                        statusplayment = rb.getText().toString();
                        break;
                    default:
                }
            }
        });

        btnPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adb.setTitle("Confirm?");
                adb.setMessage("Plese Confirm");
                adb.setNegativeButton("Ok", new AlertDialog.OnClickListener() {


                    public void onClick(DialogInterface dialog, int arg1) {
                        if (!TextUtils.isEmpty(statusplayment)|| !TextUtils.isEmpty(currentDate)) {
                            InsertData(statusplayment, currentDate);
                            mLogProgress.setMessage("loading...");
                            mLogProgress.setCanceledOnTouchOutside(false);
                            mLogProgress.show();
                            Toast.makeText(PointActivity.this, "Point Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PointActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
//                        // TODO Auto-generated method stub
//                        Toast.makeText(PointActivity.this,"Your Click OK. " ,
//                                Toast.LENGTH_LONG).show();
                    }
                });
                adb.setPositiveButton("Cancel", null);
                adb.show();



            }

            private void InsertData(String statusplayment, String currentDate) {
                reference = FirebaseDatabase.getInstance().getReference("Point");
                Map<String, Object> hashMap2 = new HashMap<>();
                temp_key = reference.push().getKey();
                reference.updateChildren(hashMap2);

                reference = reference.child(temp_key);
                Map<String, Object> add = new HashMap<>();
                add.put("id", temp_key);
                add.put("statusplayment", statusplayment);
                add.put("DateandTime", currentDate);
                reference.updateChildren(add);
            }
        });

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
        return true;
    }
}
