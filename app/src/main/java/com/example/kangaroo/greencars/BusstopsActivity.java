package com.example.kangaroo.greencars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BusstopsActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//            Fragment selectFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                    break;

//                    return true;
                case R.id.navigation_dashboard:
                    startActivity(new Intent(getApplicationContext(), BusstopsActivity.class));
                    finish();
//                    return true;
                    break;
                case R.id.navigation_notifications:
                    startActivity(new Intent(getApplicationContext(), FeedbackActivity.class));
                    finish();
//                    return true;
                    break;
                case R.id.navigation_logout:
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
//                    return true;
                    break;
            }
//            getSupportFragmentManager().beginTransaction().replace(R.id.framement_container,
//                    selectFragment).commit();
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busstops);
//        toolbar = findViewById(R.id.toolbar_feedback);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle("Greencar Stops");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }
//    @Override
//    public boolean onSupportNavigateUp() {
//        onBackPressed();
//        startActivity(new Intent(getApplicationContext(), MainActivity.class));
//        return true;
//    }
}
