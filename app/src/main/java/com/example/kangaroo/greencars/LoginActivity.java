package com.example.kangaroo.greencars;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.example.kangaroo.greencars.model.UserModel;
import com.example.kangaroo.greencars.utility.MyAlert;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity {
    private EditText edtEmail, edtPassword;
    private Button btnLogin,btnCancel;
    private TextView tvregister;
    private ProgressDialog mLogProgress;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;
    private Dialog addMemberDialog;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tvregister = findViewById(R.id.tvregister);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnCancel = findViewById(R.id.cancelButton);


        mLogProgress = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edtEmail.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();


                if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {

                    mLogProgress.setTitle("loading...");
                    mLogProgress.setMessage("Please wait while we check your credentials.");
                    mLogProgress.setCanceledOnTouchOutside(false);
                    mLogProgress.show();

                    loginUser(email, password);
                }else {
//                    Have Space
                    MyAlert myAlert = new MyAlert(LoginActivity.this);
                    myAlert.myDialog(getString(R.string.title_have_space),
                            getString(R.string.message_have_space));
                }


            }
        });

        tvregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddMemberDialog();
            }
        });
    }

    private void loginUser(String username, String password) {
        mAuth.signInWithEmailAndPassword(username, password)
                .addOnCompleteListener(task -> {


                    if (task.isSuccessful()) {
                        mLogProgress.dismiss();
                        finish();

                        firebaseUser = mAuth.getCurrentUser();
//                             assert firebaseUser != null;

                        //ดึง key มาเก็บไว้ในตัวแปล
                        String userID = firebaseUser.getUid();
                        //reference สองอันข้างล่างคือ เวลาเราจะ path เข้าไปหาข้อมูลของ user ต้องทำแบบนี้ทุกครั้ง
                        reference = FirebaseDatabase.getInstance().getReference("User").child(userID);
                        reference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    UserModel usersModel = dataSnapshot.getValue(UserModel.class);
                                    Log.d("onResume", "onDataChange: "+usersModel.getUsertype());
                                    if(usersModel.getUsertype().equals("User")){
                                        Toast.makeText(LoginActivity.this, "Usertype :"+usersModel.getUsertype(), Toast.LENGTH_SHORT).show();
                                        //  intent ไปหน้าแอดมินสร้างขึ้นมาใหม่เอง
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        finish();

                                    }

                                    mLogProgress.dismiss();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    } else {
                        mLogProgress.hide();
                         Toast.makeText(LoginActivity.this, "Cannot Sign in. ", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    }
                });
    }
    /*** dialog add members*/
    private boolean showAddMemberDialog() {
        addMemberDialog = new Dialog(LoginActivity.this);
        addMemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addMemberDialog.setContentView(R.layout.custom_member_dialog);
        addMemberDialog.setCancelable(false);

        // set dialog view
        final EditText name = (EditText) addMemberDialog.findViewById(R.id.edtFullname);
        final EditText studentID = (EditText) addMemberDialog.findViewById(R.id.edtStudentID);
        final EditText branch = (EditText) addMemberDialog.findViewById(R.id.edtBranch);
        final EditText faculty = (EditText) addMemberDialog.findViewById(R.id.edtFaculty);
        final EditText email = (EditText) addMemberDialog.findViewById(R.id.edtEmail);
        final EditText password = (EditText) addMemberDialog.findViewById(R.id.edtRegisterPassword);
        Button okButton = (Button) addMemberDialog.findViewById(R.id.btnRegisterSubmit);
        Button cancelButton = (Button) addMemberDialog.findViewById(R.id.cancelButton);


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //get data edittext
                String Name = name.getText().toString().trim();
                String StudentID = studentID.getText().toString().trim();
                String Branch = branch.getText().toString();
                String Faculty = faculty.getText().toString().trim();
                String Email = email.getText().toString().trim();
                String Password = password.getText().toString().trim();

                if (!TextUtils.isEmpty(Name) || !TextUtils.isEmpty(StudentID) || !TextUtils.isEmpty(Branch) || !TextUtils.isEmpty(Faculty) || !TextUtils.isEmpty(Email)
                        || !TextUtils.isEmpty(Password)) {
//                    mLogProgress.setTitle("Registering User");
                    mLogProgress.setMessage("loading...");
                    mLogProgress.setCanceledOnTouchOutside(false);
                    mLogProgress.show();

                    register(Name, StudentID, Branch, Faculty, Email, Password);
                    Log.d("ssa", Name);
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMemberDialog.cancel();
            }
        });

        addMemberDialog.show();
        return false;
    }

    /** register */
    private void register(final String Name, final String StudentID, final String Branch, final String Faculty, final String Email, final String Password
    ) {

        //สร้าง Email และ Password
        mAuth.createUserWithEmailAndPassword(Email, Password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            //ปิด Progress
                            mLogProgress.dismiss();

                            //เชื่อมต่อไปยังข้อมูล User
                            firebaseUser = mAuth.getCurrentUser();
                            assert firebaseUser != null;

                            //ดึง key มาเก็บไว้ในตัวแปล
                            String userID = firebaseUser.getUid();

                            //สร้างตาราง Admin ขึ้นมาเพื่อเก็บข้อมูล
                            reference = FirebaseDatabase.getInstance().getReference("User").child(userID);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userID);
                            hashMap.put("Name", Name);
                            hashMap.put("StudentID", StudentID);
                            hashMap.put("Branch", Branch);
                            hashMap.put("Faculty", Faculty);
                            hashMap.put("Email", Email);
                            hashMap.put("Password", Password);
                            hashMap.put("Usertype", "Student");
                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(LoginActivity.this, "Register Successful", Toast.LENGTH_SHORT).show();
                                        mLogProgress.setMessage("loading...");
                                        mLogProgress.setCanceledOnTouchOutside(false);
                                        mLogProgress.show();
                                        Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                    mLogProgress.dismiss();
                                }

                            });

                        } else {
                            mLogProgress.hide();
                            Toast.makeText(LoginActivity.this, "Please enter a valid email...  ", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), RegisterUserActivity.class));
                        }
                    }
                });
    }

    /**insert data **/
    private void InsertData(String Name, String StudentID, String Branch, String Faculty, String Email, String Password) {

        //สรา้ง id ให้ item
        reference = FirebaseDatabase.getInstance().getReference("User");
        Map<String, Object> hashMap2 = new HashMap<>();
        userID = reference.push().getKey();
        reference.updateChildren(hashMap2);
        reference = reference.child(userID);
        Map<String, Object> add = new HashMap<>();
        add.put("id", userID);
        add.put("Name", Name);
        add.put("StudentID", StudentID);
        add.put("Branch", Branch);
        add.put("Faculty", Faculty);
        add.put("Email", Email);
        add.put("Password", Password);
        reference.updateChildren(add);
        finish();
    }
}

