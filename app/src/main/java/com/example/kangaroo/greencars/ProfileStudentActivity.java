package com.example.kangaroo.greencars;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.kangaroo.greencars.model.Student;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileStudentActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;
    private Button btn_Edit,btnCancle;
    private TextView tv_name,tv_studentID,tv_branch,tv_faculty,tv_email,tv_pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_student);
        toolbar = findViewById(R.id.toolbar_feedback);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("                Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
        btn_Edit =findViewById(R.id.btn_Edit);


//        btnCancle =findViewById(R.id.btnCancle);

        tv_name =findViewById(R.id.tv_name);
         tv_studentID=findViewById(R.id.tv_studentID);
        tv_branch =findViewById(R.id.tv_branch);
        tv_faculty =findViewById(R.id.tv_faculty);
        tv_email =findViewById(R.id.tv_email);
        tv_pass =findViewById(R.id.tv_pass);




        firebaseUser = mAuth.getCurrentUser();
        final String id = firebaseUser.getUid();
        //path เข้าไปในข้อมูล Users

        reference = FirebaseDatabase.getInstance().getReference("User").child(id);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                final Student usersModel = dataSnapshot.getValue(Student.class);
                String text = dataSnapshot.child("Email").getValue(String.class);
                tv_email.setText(usersModel.getEmail());
                Log.d("Email", text);
                tv_pass.setText(usersModel.getPassword());
                tv_name.setText(usersModel.getName());
                tv_studentID.setText(usersModel.getStudentID());
                tv_branch.setText(usersModel.getBranch());
                tv_faculty.setText(usersModel.getFaculty());

                btn_Edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), EditStudentDataActivity.class);
                        intent.putExtra("Email", usersModel.getEmail());
                        intent.putExtra("Password", usersModel.getPassword());
                        intent.putExtra("Name", usersModel.getName());
                        intent.putExtra("StudentID", usersModel.getStudentID());
                        intent.putExtra("Branch", usersModel.getBranch());
                        intent.putExtra("Faculty", usersModel.getFaculty());
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });



    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
        return true;
    }
}


