package com.example.kangaroo.greencars.model;

public class Student {
    private String Name;
    private String StudentID;
    private String Branch;
    private String Faculty;
    private String Email;
    private String Password;
    private String id;

    public Student() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String StudentID) {
        this.StudentID = StudentID;
    }
    public String getBranch() {
        return Branch;
    }

    public void setBranch(String Branch) {
        this.Branch = Branch;
    }

    public String getFaculty() {
        return Faculty;
    }

    public void setFaculty(String Faculty) {
        this.Faculty = Faculty;
    }
    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) { this.Email = Email; }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) { this.Password = Password; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
