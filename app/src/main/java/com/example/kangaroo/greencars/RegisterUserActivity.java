package com.example.kangaroo.greencars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kangaroo.greencars.utility.MyAlert;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegisterUserActivity extends AppCompatActivity {
    private EditText edtFullname, edtStudentID, edtBranch, edtFaculty, edtEmail, edtRegisterPassword;
    private Button btnRegisterSubmit;
    private ProgressDialog mLogProgress;
    private DatabaseReference reference;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    String userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_member_dialog);
        edtFullname = findViewById(R.id.edtFullname);
        edtStudentID = findViewById(R.id.edtStudentID);
        edtBranch = findViewById(R.id.edtBranch);
        edtFaculty = findViewById(R.id.edtFaculty);
        edtEmail = findViewById(R.id.edtEmail);
        edtRegisterPassword = findViewById(R.id.edtRegisterPassword);
        btnRegisterSubmit = findViewById(R.id.btnRegisterSubmit);

        mAuth = FirebaseAuth.getInstance();
        mLogProgress = new ProgressDialog(this);
        btnRegisterSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ดึงค่าจาก Edittext
                String Name = edtFullname.getText().toString().trim();
                String StudentID = edtStudentID.getText().toString().trim();
                String Branch = edtBranch.getText().toString();
                String Faculty = edtFaculty.getText().toString().trim();
                String Email = edtEmail.getText().toString().trim();
                String Password = edtRegisterPassword.getText().toString().trim();

                if (!TextUtils.isEmpty(Name) || !TextUtils.isEmpty(StudentID) || !TextUtils.isEmpty(Branch) || !TextUtils.isEmpty(Faculty) || !TextUtils.isEmpty(Email)
                        || !TextUtils.isEmpty(Password)) {
//                    mLogProgress.setTitle("Registering User");
                    mLogProgress.setMessage("loading...");
                    mLogProgress.setCanceledOnTouchOutside(false);
                    mLogProgress.show();

                    register(Name, StudentID, Branch, Faculty, Email, Password);
                }else {
//                    Have Space
                    MyAlert myAlert = new MyAlert(RegisterUserActivity.this);
                    myAlert.myDialog(getString(R.string.title_have_space),
                            getString(R.string.message_have_register));
                }

            }
        });

    }

    private void register(final String Name, final String StudentID, final String Branch, final String Faculty, final String Email, final String Password
    ) {

        //สร้าง Email และ Password
        mAuth.createUserWithEmailAndPassword(Email, Password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            //ปิด Progress
                            mLogProgress.dismiss();

                            //เชื่อมต่อไปยังข้อมูล User
                            firebaseUser = mAuth.getCurrentUser();
                            assert firebaseUser != null;

                            //ดึง key มาเก็บไว้ในตัวแปล
                            String userID = firebaseUser.getUid();

                            //สร้างตาราง Admin ขึ้นมาเพื่อเก็บข้อมูล
                            reference = FirebaseDatabase.getInstance().getReference("User").child(userID);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userID);
                            hashMap.put("Name", Name);
                            hashMap.put("StudentID", StudentID);
                            hashMap.put("Branch", Branch);
                            hashMap.put("Faculty", Faculty);
                            hashMap.put("Email", Email);
                            hashMap.put("Password", Password);
                            hashMap.put("Usertype", "Student");
                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(RegisterUserActivity.this, "Register Successful", Toast.LENGTH_SHORT).show();
                                        mLogProgress.setMessage("loading...");
                                        mLogProgress.setCanceledOnTouchOutside(false);
                                        mLogProgress.show();
                                        Intent intent = new Intent(RegisterUserActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                    mLogProgress.dismiss();
                                }

                            });

                        } else {
                            mLogProgress.hide();
                            Toast.makeText(RegisterUserActivity.this, "Please enter a valid email...  ", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), RegisterUserActivity.class));

                        }



                    }
                });
    }
    private void InsertData(String Name, String StudentID, String Branch, String Faculty, String Email, String Password) {

        //สรา้ง id ให้ item
        reference = FirebaseDatabase.getInstance().getReference("User");
        Map<String, Object> hashMap2 = new HashMap<>();
        userID = reference.push().getKey();
        reference.updateChildren(hashMap2);
        reference = reference.child(userID);
        Map<String, Object> add = new HashMap<>();
        add.put("id", userID);
        add.put("Name", Name);
        add.put("StudentID", StudentID);
        add.put("Branch", Branch);
        add.put("Faculty", Faculty);
        add.put("Email", Email);
        add.put("Password", Password);
        reference.updateChildren(add);
        finish();
    }
}
