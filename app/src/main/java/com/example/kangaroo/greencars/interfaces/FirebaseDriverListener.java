package com.example.kangaroo.greencars.interfaces;


import com.example.kangaroo.greencars.model.Driver;

public interface FirebaseDriverListener {

    void onDriverAdded(Driver driver);

    void onDriverRemoved(Driver driver);

    void onDriverUpdated(Driver driver);

}
