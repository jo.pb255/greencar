package com.example.kangaroo.greencars.utility;


import android.content.Context;
import android.content.DialogInterface;


import androidx.appcompat.app.AlertDialog;

import com.example.kangaroo.greencars.R;


public class MyAlert {

    private Context myContext;

    public MyAlert(Context myContext) {
        this.myContext = myContext;
    }

    public void myDialog(String strTitle, String strMessage) {

        AlertDialog.Builder builder = new AlertDialog.Builder(myContext);
        builder.setCancelable(false);
        builder.setIcon(R.drawable.complain);
        builder.setTitle(strTitle);
        builder.setMessage(strMessage);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}