package com.example.kangaroo.greencars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.kangaroo.greencars.model.Student;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {
    private EditText edtNameFb, edtEmailFd, edtMessageFd;
    private Button btnSubmitFeedback;
    private ProgressDialog mLogProgress;
    private DatabaseReference reference;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private Toolbar toolbar;
    private RatingBar UserratingBar;
    String FeedbackID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        toolbar = findViewById(R.id.toolbar_feedback);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("                  Feedback");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        UserratingBar = findViewById(R.id.UserratingBar);
        edtNameFb = findViewById(R.id.edtNameFb);
        edtEmailFd = findViewById(R.id.edtEmailFd);
        edtMessageFd = findViewById(R.id.edtMessageFd);
        btnSubmitFeedback = findViewById(R.id.btnSubmitFeedback);

        mAuth = FirebaseAuth.getInstance();
        mLogProgress = new ProgressDialog(this);

        firebaseUser = mAuth.getCurrentUser();
        final String id = firebaseUser.getUid();

        reference = FirebaseDatabase.getInstance().getReference("User").child(id);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                final Student usersModel = dataSnapshot.getValue(Student.class);
                String text = dataSnapshot.child("Email").getValue(String.class);
                edtEmailFd.setText(usersModel.getEmail());
                Log.d("Email", text);

                edtNameFb.setText(usersModel.getName());

                btnSubmitFeedback.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        //ดึงค่าจาก Edittext
                        String Name = edtNameFb.getText().toString();
                        String Email = edtEmailFd.getText().toString();
                        String Messagefb = edtMessageFd.getText().toString();

                        float score = UserratingBar.getRating();


                        if (!TextUtils.isEmpty(Name) || !TextUtils.isEmpty(Email) || !TextUtils.isEmpty(Messagefb)) {
                            InsertData(Name, Email, Messagefb,score);

                            Toast.makeText(FeedbackActivity.this, "Feedback Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(FeedbackActivity.this, FeedbackActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        btnSubmitFeedback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //ดึงค่าจาก Edittext
                String Name = edtNameFb.getText().toString();
                String Email = edtEmailFd.getText().toString();
                String Messagefb = edtMessageFd.getText().toString();


                float score = UserratingBar.getRating();
                if (!TextUtils.isEmpty(Name) || !TextUtils.isEmpty(Email) || !TextUtils.isEmpty(Messagefb)) {
                    InsertData(Name, Email, Messagefb,score);

                    Toast.makeText(FeedbackActivity.this, "Feedback Successful", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(FeedbackActivity.this, FeedbackActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

//                    finish();
//                    mLogProgress.setMessage("loading...");
//                    mLogProgress.setCanceledOnTouchOutside(false);
//                    mLogProgress.show();

                }

            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
        return true;
    }

    private void InsertData(String Name, String Email, String Messagefb, float score) {

        //สรา้ง id ให้ item
        reference = FirebaseDatabase.getInstance().getReference("Feedback");
        Map<String, Object> hashMap2 = new HashMap<>();
        FeedbackID = reference.push().getKey();
        reference.updateChildren(hashMap2);
        reference = reference.child(FeedbackID);
        Map<String, Object> add = new HashMap<>();
        add.put("id", FeedbackID);
        add.put("Name", Name);
        add.put("Email", Email);
        add.put("Messagefb", Messagefb);
        add.put("Rating", score);

        reference.updateChildren(add);
        finish();
    }


}
