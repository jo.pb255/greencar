package com.example.kangaroo.greencars;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.text.SelectFormat;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;

import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.kangaroo.greencars.collection.MarkerCollection;
import com.example.kangaroo.greencars.helpers.FirebaseEventListenerHelper;
import com.example.kangaroo.greencars.helpers.GoogleMapHelper;
import com.example.kangaroo.greencars.helpers.MarkerAnimationHelper;
import com.example.kangaroo.greencars.helpers.UiHelper;
import com.example.kangaroo.greencars.interfaces.FirebaseDriverListener;
import com.example.kangaroo.greencars.interfaces.LatLngInterpolator;
import com.example.kangaroo.greencars.model.Driver;

import com.example.kangaroo.greencars.model.Student;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.*;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.nio.channels.Selector;


public class MainActivity extends AppCompatActivity implements FirebaseDriverListener,
        GoogleMap.OnPolylineClickListener
        {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2161;
    private static final  int LOCATION_REQUEST_CODE = 100;
    private static final String ONLINE_DRIVERS = "online_drivers";
    private FirebaseAuth mAuth;
    private final GoogleMapHelper googleMapHelper = new GoogleMapHelper();
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(ONLINE_DRIVERS);
    private FloatingActionButton btn_Floating;
    private GoogleMap googleMap;
    private LocationRequest locationRequest;
    private UiHelper uiHelper;
    private FirebaseEventListenerHelper firebaseEventListenerHelper;
    private FusedLocationProviderClient locationProviderClient;

    private Marker currentLocationMarker;

    private TextView totalOnlineDrivers;
    private boolean locationFlag = true;
    private static final int COLOR_GREEN_ARGB = 0xff388E3C;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//            Fragment selectFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                    break;


//                    return true;
                case R.id.navigation_dashboard:
                    startActivity(new Intent(getApplicationContext(), BusstopsActivity.class));
                    finish();
//                    return true;
                    break;
                case R.id.navigation_notifications:
                    startActivity(new Intent(getApplicationContext(), FeedbackActivity.class));
                    finish();
//                    return true;
                    break;
                case R.id.navigation_profile:
                    startActivity(new Intent(getApplicationContext(), ProfileStudentActivity.class));
                    finish();
//                    return true;
                    break;
                case R.id.navigation_logout:
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

//                    return true;
                    break;
            }
//            getSupportFragmentManager().beginTransaction().replace(R.id.framement_container,
//                    selectFragment).commit();
            return false;
        }
    };



    private LocationCallback locationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            Location location = locationResult.getLastLocation();
            if (location == null) return;
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            if (locationFlag) {
                locationFlag = false;
                animateCamera(latLng);
            }
//            showOrAnimateUserMarker(latLng);
            onMapReady(googleMap);


        }
    };

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_Floating =findViewById(R.id.btn_Floating);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        btn_Floating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),PointActivity.class));
            }
        });

//        mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainActivity.this);
        if (status == ConnectionResult.SUCCESS){
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            }
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.supportMap);

        uiHelper = new UiHelper(this);
        assert mapFragment != null;
        mapFragment.getMapAsync(googleMap -> this.googleMap = googleMap);
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = uiHelper.getLocationRequest();
        if (!uiHelper.isPlayServicesAvailable()) {
            Toast.makeText(this, "Play Services did not installed!", Toast.LENGTH_SHORT).show();
            finish();
        } else requestLocationUpdates();
        totalOnlineDrivers = findViewById(R.id.totalOnlineDrivers);

        firebaseEventListenerHelper = new FirebaseEventListenerHelper(this);
        databaseReference.addChildEventListener(firebaseEventListenerHelper);
//        finish();

        //เช็คค่า
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }



    }

    //showlocation user and markers in map
    private void onMapReady(GoogleMap googleMap) {
        googleMap = googleMap;
        LatLng Rmu = new LatLng(16.197338, 103.274106);
        Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(16.199840, 103.272924),
                        new LatLng(16.199881, 103.276001),
                        new LatLng(16.197267, 103.276249),
                        new LatLng(16.195879, 103.275896),
                        new LatLng(16.195374, 103.274128),
                        new LatLng(16.195342, 103.272208),
                        new LatLng(16.197085, 103.272130),
                        new LatLng(16.197312, 103.273325),
                        new LatLng(16.199851, 103.272930),
                        new LatLng(16.200356, 103.271553),
                        new LatLng(16.201008, 103.271613),
                        new LatLng(16.201115, 103.273001),
                        new LatLng(16.200381, 103.272935),
                        new LatLng(16.200463, 103.271851),
                        new LatLng(16.200986, 103.271882),
                        new LatLng(16.201008, 103.271613),
                        new LatLng(16.201115, 103.273001))
        .width(5)
        .color(COLOR_GREEN_ARGB));
        googleMap.setOnPolylineClickListener(this);




        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Rmu, 16));

        LatLng Station = new LatLng(16.197342, 103.276975);
        googleMap.addMarker(new MarkerOptions().position(Station)
                .title("Bus Station")
                .snippet("Start time 09:00 - 10:00 - 11:00 - 13:00 - 14:00 - 15:00")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.iconbusstop)));
        LatLng a = new LatLng(16.195879, 103.275896);
        googleMap.addMarker(new MarkerOptions().position(a)
                .title("BusStop")
                .snippet("จุดจอดหน้าร้าน Milk Me Coffee&Cafe")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng b = new LatLng(16.195374, 103.274128);
        googleMap.addMarker(new MarkerOptions().position(b)
                .title("BusStop")
                .snippet("จุดจอดหน้าอาคาร 36 คณะมนุษยศาสตร์และสังคมศาสตร์")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng c = new LatLng(16.195342, 103.272208);
        googleMap.addMarker(new MarkerOptions().position(c)
                .title("BusStop")
                .snippet("จุดจอดหน้าหน้าโรงเรียนอนุบาลสาธิต")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng d = new LatLng(16.197085, 103.272130);
        googleMap.addMarker(new MarkerOptions().position(d)
                .title("BusStop")
                .snippet("จุดจอดหน้าหน้าอาคารหอพักในนักศึกษา")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng e = new LatLng(16.198359, 103.273082);
        googleMap.addMarker(new MarkerOptions().position(e)
                .title("BusStop")
                .snippet("จุดจอดหน้าอาคาร 8 สาขาคอมพิวเตอร์ศึกษา")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng f = new LatLng(16.199913, 103.271764);
        googleMap.addMarker(new MarkerOptions().position(f)
                .title("BusStop")
                .snippet("จุดจอดหน้าสำนักวิทยบริการและเทคโนโลยีสารสนเทศ")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng g = new LatLng(16.200795, 103.271439);
        googleMap.addMarker(new MarkerOptions().position(g)
                .title("BusStop")
                .snippet("จุดจอดหน้าอาคาร 15 อาคารเฉลิมพระเกียรติ 72 พรรษา")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng h = new LatLng(16.200964, 103.272916);
        googleMap.addMarker(new MarkerOptions().position(h)
                .title("BusStop")
                .snippet("จุดจอดอาคาร 38 คณะเทคโนโลยีสารสนเทศ")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng i = new LatLng(16.199814, 103.274113);
        googleMap.addMarker(new MarkerOptions().position(i)
                .title("BusStop")
                .snippet("จุดจอดหน้ารอาคารกองกิจการนักศึกษา")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
        LatLng j = new LatLng(16.198004, 103.276356);
        googleMap.addMarker(new MarkerOptions().position(j)
                .title("BusStop")
                .snippet("จุดจอดหน้าสระว่ายน้ำ")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.busstop)));
//
//        if (googleMap != null){
//            if (ActivityCompat.checkSelfPermission(MainActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED){
//                ActivityCompat.requestPermissions(MainActivity.this,
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
//            } else {
////                googleMap.setMyLocationEnabled(true);
////                googleMap.getUiSettings().setZoomControlsEnabled(true);
//
//            }
//        }


    }

    private void animateCamera(LatLng latLng) {
        CameraUpdate cameraUpdate = googleMapHelper.buildCameraUpdate(latLng);
        googleMap.animateCamera(cameraUpdate);

    }

//    private void showOrAnimateUserMarker(LatLng latLng){
//        if (currentLocationMarker == null)
//            currentLocationMarker = googleMap.addMarker(googleMapHelper.getCurrentLocationMarker(latLng));
//        else
//            MarkerAnimationHelper.animateMarkerToGB(
//                    currentLocationMarker,
//                    new LatLng(latLng.latitude,
//                            latLng.longitude),
//                    new LatLngInterpolator.Spherical());
//    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdates() {
        if (!uiHelper.isHaveLocationPermission()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
        if (uiHelper.isLocationProviderEnabled())
            uiHelper.showPositiveDialogWithListener(this, getResources().getString(R.string.need_location), getResources().getString(R.string.location_content), () -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)), "Turn On", false);
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            int value = grantResults[0];
            if (value == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Location Permission denied", Toast.LENGTH_SHORT).show();
                finish();
            } else if (value == PackageManager.PERMISSION_GRANTED) requestLocationUpdates();
        }
    }

    @Override
    public void onDriverAdded(Driver driver) {
        MarkerOptions markerOptions = googleMapHelper.getDriverMarkerOptions(new LatLng(driver.getLat(), driver.getLng()));
        Marker marker = googleMap.addMarker(markerOptions);
        marker.setTag(driver.getDriverId());
        MarkerCollection.insertMarker(marker);
        totalOnlineDrivers.setText(getResources()
                .getString(R.string.total_online_greencar)
                .concat(" ")
                .concat(String
                        .valueOf(MarkerCollection
                                .allMarkers()
                                .size())));

    }

    @Override
    public void onDriverRemoved(Driver driver) {
        MarkerCollection.removeMarker(driver.getDriverId());
        totalOnlineDrivers.setText(getResources()
                .getString(R.string.total_online_greencar)
                .concat(" ")
                .concat(String
                        .valueOf(MarkerCollection
                                .allMarkers()
                                .size())));
    }

    @Override
    public void onDriverUpdated(Driver driver) {
        Marker marker = MarkerCollection.getMarker(driver.getDriverId());
        assert marker != null;
        MarkerAnimationHelper.animateMarkerToGB(marker, new LatLng(driver.getLat(), driver.getLng()), new LatLngInterpolator.Spherical());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseReference.removeEventListener(firebaseEventListenerHelper);
        locationProviderClient.removeLocationUpdates(locationCallback);
        MarkerCollection.clearMarkers();
        currentLocationMarker = null;

    }

    @Override
    public void onPolylineClick(Polyline polyline) {

    }
}
